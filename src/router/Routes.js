import OrganizationTable from "../pages/branches/List."
import BranchForm from "../pages/branches/BranchForm"
import { voxitPath } from "../router/Path"

export const routes = [
	{
		path: voxitPath.Home,
		component: OrganizationTable,
		exact: true,
	},
	{
		path: voxitPath.NEWBRANCHFORM,
		component: BranchForm,
		exact: true,
	},
]
