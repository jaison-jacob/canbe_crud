export const voxitPath = {
	login: "/home",
	applyJockey: "/jockey",
	Home: "/",
	NEWBRANCHFORM: "/branchNewForm",
	OVERALLANALYTICS: "/home/Dashboard/OverallAnalytics",
	USER_ANALYTICS: "/home/Dashboard/OverallAnalytics",
	Jockey_ANALYTICS: "/home/Dashboard/OverallAnalytics",
	AUDIO_ANALYTICS: "/home/Dashboard/OverallAnalytics",
	VCORNER_ANALYTICS: "/home/Dashboard/OverallAnalytics",
	NOTIFICATION_ANALYTICS: "/home/Dashboard/OverallAnalytics",
	Listener: "/home/Users/Listener",
	JOCKEY_APPLIES: "/home/Users/JockeyApplies",
	JOCKEYS: "/home/Users/Jockeys",
	AUDIOS_FROM_JOCKEY: "/home/Audios/AudioFromJockey",
	READY_FOR_LIVE: "/home/Audios/ReadyForLive",
	LIVE_AUDIOS: "/home/Audios/LiveAudios",
	TRENDING: "/home/AdminModule/Trending",
	BANNER: "/home/AdminModule/Banner",
	GENRE: "/home/AdminModule/Genre",
	LANGUAGE: "/home/AdminModule/Language",
	POP_UP_IMAGE: "/home/AdminModule/PopUpImage",
	NOTIFICATION: "/home/AdminModule/Notification",
	VCORNER_POINT: "/home/Vcorner/VcornerPoint",
	QUIZ: "/home/Vcorner/Quiz",
	POLLS: "/home/Vcorner/Polls",
	COMPANY: "/home/AdCampaign/Company",
	CAMPAIGN_PLAN: "/home/AdCampaign/CampainePlan",
}
