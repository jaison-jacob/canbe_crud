// import { Link } from 'react-router-dom';
// import {todo_Edit,todo_Delete} from '../../redux/action'
import { Avatar, Button, IconButton, Typography } from "@material-ui/core"
import SortIcon from "@material-ui/icons/Sort"
import { Box } from "@material-ui/core"
import { format } from "date-fns"
import imagepatt from "../asserts/Ellipse 1.png"
// import DOTT from "./assets/Group 1794 (1).svg";
import PrfilePopUp from "../components/branches/branchtable/ProfilePopUp"
import "../styles/OrganizationTable.scss"
import ColumnHideCheckBox from "../components/branches/branchtable/ColumnHideCheckBox"
// import classes from "*.module.css";

export const COLUMNS = () => [
	{
		Header: () => (
			<div
				style={{
					display: "flex",
					alignItems: "center",
					zIndex: 100,
				}}
			>
				<ColumnHideCheckBox />

				<Typography
					component='h6'
					style={{ fontSize: 14, margin: "0 auto", marginLeft: "30%" }}
				>
					Institute Name
				</Typography>
			</div>
		),
		accessor: "arganizationName",
		sticky: "left",
		width: 346,
		minWidth: 200,
		maxWidth: 450,
		Cell: (orgin) => {
			console.log(orgin.row.index)
			return (
				<div
					style={{
						display: "flex",
						justifyContent: "space-between",
						alignItems: "center",
						color: "#000000DE",
						width: "100%",
					}}
				>
					<div style={{ display: "flex", alignItems: "center", width: "40%" }}>
						<Avatar src={imagepatt} style={{ width: 32, height: 32 }} />
						<Typography
							component='h6'
							style={{ fontSize: 14, marginLeft: "30%" }}
						>
							{orgin.cell.value}
						</Typography>
					</div>
					<PrfilePopUp index={orgin.row.index} />
				</div>
			)
		},
	},
	{
		Header: "Branch Name",
		accessor: "branchName",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#0000008A",
	},
	{
		Header: "Branch Address",
		accessor: "branchAddress",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#000000B8",
	},
	{
		Header: "Country",
		accessor: "Country",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#0000008A",
	},
	{
		Header: "Status",
		accessor: "Status",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "green",
	},
	{
		Header: "State",
		accessor: "State",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#000000B8",
	},

	{
		Header: "Pincode",
		accessor: "Pincode",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#000000B8",
	},
	{
		Header: "Latitude",
		accessor: "Latitude",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#000000B8",
	},
	{
		Header: "Longitude",
		accessor: "Longitude",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#000000B8",
	},
	{
		Header: "LandMark",
		accessor: "LandMark",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#000000B8",
	},
	{
		Header: "Distance From LandMark",
		accessor: "distanceFromLandMark",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#000000B8",
	},
	{
		Header: "Nearest",
		accessor: "Nearest",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#000000B8",
	},
	{
		Header: "Effective Date",
		accessor: "effectiveDate",
		Cell: ({ value }) => {
			return format(new Date(value), "dd/MM/yyyy")
		},
		width: 250,
		minWidth: 200,
		maxWidth: 300,
	},
	{
		Header: "Reason For Deactive",
		accessor: "reasonForDeactive",
		width: 250,
		minWidth: 200,
		maxWidth: 300,
		color: "#000000B8",
	},
]
