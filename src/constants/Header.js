import { voxitPath } from "../router/Path"

export const menuTitle = [
	{
		titleName: "Dashboard",
		menuItem: [
			{
				subMenuItemName: "Overall analytics",
				subMenuItemPath: voxitPath.OVERALL_ANALYTICS,
			},
			{
				subMenuItemName: "User analytics",
				subMenuItemPath: voxitPath.USER_ANALYTICS,
			},
			{
				subMenuItemName: "Jockey analytics",
				subMenuItemPath: voxitPath.Jockey_ANALYTICS,
			},
			{
				subMenuItemName: "Audio analytics",
				subMenuItemPath: voxitPath.AUDIO_ANALYTICS,
			},
			{
				subMenuItemName: "Vcorner analytics",
				subMenuItemPath: voxitPath.VCORNER_ANALYTICS,
			},
			{
				subMenuItemName: "Notification analytics",
				subMenuItemPath: voxitPath.NOTIFICATION_ANALYTICS,
			},
		],
		key: "Dashboard",
	},
	{
		titleName: "Users",
		menuItem: [
			{
				subMenuItemName: "Listener",
				subMenuItemPath: voxitPath.Listener,
			},
			{
				subMenuItemName: "Jockey Applies",
				subMenuItemPath: voxitPath.JOCKEY_APPLIES,
			},
			{
				subMenuItemName: "Jockeys",
				subMenuItemPath: voxitPath.JOCKEYS,
			},
		],
		key: "Users",
	},
	{
		titleName: "Audios",
		menuItem: [
			{
				subMenuItemName: "Audio from jockey",
				subMenuItemPath: voxitPath.AUDIOS_FROM_JOCKEY,
			},
			{
				subMenuItemName: "Ready for live",
				subMenuItemPath: voxitPath.READY_FOR_LIVE,
			},
			{
				subMenuItemName: "Live Audios",
				subMenuItemPath: voxitPath.LIVE_AUDIOS,
			},
		],
		key: "Audios",
	},
	{
		titleName: "Admin Module",
		menuItem: [
			{
				subMenuItemName: "Trending",
				subMenuItemPath: voxitPath.TRENDING,
			},
			{
				subMenuItemName: "Banner",
				subMenuItemPath: voxitPath.BANNER,
			},
			{
				subMenuItemName: "Genre",
				subMenuItemPath: voxitPath.GENRE,
			},
			{
				subMenuItemName: "Language",
				subMenuItemPath: voxitPath.LANGUAGE,
			},
			{
				subMenuItemName: "Pop Up Image",
				subMenuItemPath: voxitPath.POP_UP_IMAGE,
			},
			{
				subMenuItemName: "Notification",
				subMenuItemPath: voxitPath.NOTIFICATION,
			},
		],
		key: "AdminModule",
	},
	{
		titleName: "Vcorner",
		menuItem: [
			{
				subMenuItemName: "Vcorner Points",
				subMenuItemPath: voxitPath.VCORNER_POINT,
			},
			{
				subMenuItemName: "Quiz",
				subMenuItemPath: voxitPath.QUIZ,
			},
			{
				subMenuItemName: "Polls",
				subMenuItemPath: voxitPath.POLLS,
			},
		],
		key: "Vcorner",
	},
	{
		titleName: "Ad Campaign",
		menuItem: [
			{
				subMenuItemName: "Company",
				subMenuItemPath: voxitPath.COMPANY,
			},
			{
				subMenuItemName: "Campaine Plan",
				subMenuItemPath: voxitPath.CAMPAIGN_PLAN,
			},
		],
		key: "AdCampaign",
	},
]

export const profileItem = [
	{
		profiletext: "Roles & Access",
	},
	{
		profiletext: "Our Employeees",
	},
	{
		profiletext: "Jockey Catagories",
	},
	{
		profiletext: "Prameters",
	},
	{
		profiletext: "Themes",
	},
	{
		profiletext: "Logout",
		active: true,
	},
]
