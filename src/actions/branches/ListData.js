let initialState = {
	branchListData: [],
	edit: null,
	editIndex: null,
	profileView: false,
}
export const CHANGE_STATE = "CHANGE_STATE"

export const createListData = (value, status) => {
	console.log(value)
	return async (dispatch, getState) => {
		let copybranchListData = { ...getState().branchListReducer }
		let createbranchListObject = {}

		for (const item in value) {
			createbranchListObject[item] = value[item].value
		}
		console.log(createbranchListObject)
		if (status === "add") {
			copybranchListData.branchListData.push(createbranchListObject)
		} else {
			copybranchListData.branchListData[
				copybranchListData.editIndex
			] = createbranchListObject
			copybranchListData.edit = null
			copybranchListData.editIndex = null
		}

		console.log(copybranchListData)
		dispatch({ type: CHANGE_STATE, value: copybranchListData })
	}
}
export const Edit = (index, status) => {
	return async (dispatch, getState) => {
		let copybranchListData = { ...getState().branchListReducer }
		if (status === "Edit") {
			copybranchListData = {
				...copybranchListData,
				edit: copybranchListData.branchListData[index],
				editIndex: index,
				profileView: false,
			}
		} else {
			copybranchListData = {
				...copybranchListData,
				edit: copybranchListData.branchListData[index],
				editIndex: index,
				profileView: true,
			}
		}

		console.log(copybranchListData)
		dispatch({ type: CHANGE_STATE, value: copybranchListData })
	}
}
export const inActiveProfile = (index) => {
	return async (dispatch, getState) => {
		let copybranchListData = { ...getState().branchListReducer }
		console.log(copybranchListData.branchListData.splice(0, 1))
		console.log(copybranchListData)
		dispatch({ type: CHANGE_STATE, value: copybranchListData })
	}
}

export const branchListReducer = (state = initialState, action) => {
	console.log("calling")
	switch (action.type) {
		case CHANGE_STATE:
			return { ...action.value }
		default:
			return state
	}
}
