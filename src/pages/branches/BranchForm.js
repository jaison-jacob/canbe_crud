import { Container } from "@material-ui/core"
import React from "react"
import BranchFormComponent from "../../components/branches/branchform/BranchFormComponent"

function BranchForm(props) {
	console.log(props)
	return (
		<Container>
			<BranchFormComponent />
		</Container>
	)
}

export default BranchForm
