import React, { useMemo } from "react"
import { COLUMNS } from "../../constants/Column"
import {
	useTable,
	useSortBy,
	useGlobalFilter,
	usePagination,
	useBlockLayout,
	useResizeColumns,
} from "react-table"
import { useSticky } from "react-table-sticky"
import GlobalFilter from "../../components/branches/branchtable/GlobalFilter"
import Pagination from "../../components/branches/branchtable/Pagination"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import * as Actions from "../../actions/branches/ListData"
import ListBody from "../../components/branches/branchtable/ListBody"
import "../../styles/OrganizationTable.scss"

function List(props) {
	console.log(props.value)
	const columns = useMemo(() => COLUMNS(), [])
	const modifiedData = useMemo(() => props.value, [])
	const defaultColumn = React.useMemo(
		() => ({
			minWidth: 30,
			width: 150,
			maxWidth: 400,
		}),
		[]
	)
	const tableInstance = useTable(
		{
			columns,
			data: modifiedData,
			defaultColumn,
		},
		useGlobalFilter,
		useSortBy,
		usePagination,
		useBlockLayout,
		useSticky,
		useResizeColumns
	)

	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		page,
		nextPage,
		previousPage,
		canPreviousPage,
		canNextPage,
		pageOptions,
		gotoPage,
		pageCount,
		setPageSize,
		prepareRow,
		state,
		setGlobalFilter,
	} = tableInstance

	const { globalFilter } = state
	const { pageIndex, pageSize } = state

	const footerGroups = page.slice(0, 20)
	if (!props.value.length) {
		return (
			<div className='tableContainer'>
				<div>
					<GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
				</div>
				<p>no dsta found</p>
			</div>
		)
	}
	return (
		<div className='tableContainer'>
			<div>
				<GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
			</div>

			<div>
				<div
					{...getTableProps()}
					className='table sticky'
					style={{ width: 1000, height: 400 }}
				>
					<ListBody
						headerGroups={headerGroups}
						getTableBodyProps={getTableBodyProps}
						footerGroups={footerGroups}
						prepareRow={prepareRow}
					/>
				</div>
			</div>
			<div className='pagination_container'>
				<Pagination
					pageIndex={pageIndex}
					pageCount={pageCount}
					nextPage={nextPage}
					previousPage={previousPage}
					setPageSize={setPageSize}
					pageSize={pageSize}
					pageOptions={pageOptions}
				/>
			</div>
		</div>
	)
}

const mapDispatchToprops = (dispatch) => {
	return bindActionCreators(
		{
			createListData: Actions.createListData,
		},
		dispatch
	)
}
const mapStateToProps = (state) => {
	console.log(state)
	return {
		value: [...state.branchListReducer.branchListData],
	}
}

export default connect(mapStateToProps, mapDispatchToprops)(List)
