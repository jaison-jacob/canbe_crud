import React from "react"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import { routes } from "../router/Routes"
import Header from "../components/reusable/Header"

function Layout() {
	return (
		<div>
			<Header />
			<Router>
				<Switch>
					{routes.map((item, index) => (
						<Route
							key={index}
							path={item.path}
							component={item.component}
							exact={item.exact}
						/>
					))}
				</Switch>
			</Router>
		</div>
	)
}

export default Layout
