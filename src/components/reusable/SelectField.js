import React from "react"
import { FormControl, InputLabel, Select, MenuItem } from "@material-ui/core"

function SelectField(props) {
	console.log(props.name, props.style)
	// clsx(classes.select_Style, "form_Text_Style")
	return (
		<div style={props.style}>
			<FormControl
				variant='outlined'
				className={props.className}
				name={props.name}
				fullWidth
			>
				<InputLabel id='demo-simple-select-outlined-label'>
					{props.label}
				</InputLabel>
				<Select
					labelId='demo-simple-select-outlined-label'
					id='demo-simple-select-outlined'
					value={props.value || ""}
					onChange={props.onChange}
					label={props.label}
					name={props.name}
					disabled={props.disabled}
				>
					{props.options?.map((e, index) => (
						<MenuItem value={e || ""} key={index}>
							{e}
						</MenuItem>
					))}
				</Select>
			</FormControl>
		</div>
	)
}

export { SelectField }
