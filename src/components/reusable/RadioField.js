import React from "react"
import {
	Radio,
	FormLabel,
	FormControl,
	RadioGroup,
	FormControlLabel,
} from "@material-ui/core"

function RadioField(props) {
	return (
		<div
			style={{
				display: "flex",
				alignItems: "center",
				paddingBottom: 20,
			}}
		>
			<FormLabel component='legend' className={props.className}>
				{props.label}
			</FormLabel>
			<FormControl component='fieldset' classes={props.classes}>
				<RadioGroup
					aria-label='gender'
					name={props.name}
					value={props.value || ""}
					onChange={props.onChange}
					row
				>
					{props.options.map((e) => (
						<FormControlLabel
							value={e || ""}
							control={<Radio color='primary' />}
							label={<span className={props.labelClassname}>{e || ""}</span>}
							disabled={props.disabled}
						/>
					))}
				</RadioGroup>
			</FormControl>
		</div>
	)
}

export { RadioField }
