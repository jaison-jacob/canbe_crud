import React from "react"
import { makeStyles } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
	formControl: {
		display: "flex",
		flexWrap: "wrap",
		justifyContent: "space-between",
	},
}))

function FormWrapper(props) {
	const styles = useStyles()
	return <div className={styles.formControl}>{props.children}</div>
}

export { FormWrapper }
