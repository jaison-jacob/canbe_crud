import React, { useState } from "react"
import { makeStyles } from "@material-ui/core/styles"
import Toolbar from "@material-ui/core/Toolbar"
import IconButton from "@material-ui/core/IconButton"
import MenuItem from "@material-ui/core/MenuItem"
import Menu from "@material-ui/core/Menu"
import Avatar from "@material-ui/core/Avatar"
import { Box, Typography, Button } from "@material-ui/core"
import vaduvelu from "../../asserts/vadivelu-best-hd-photos-download-1080p-whatsapp-dpstatus-images-uksj-550x366.jpeg"
import HeasderImg from "../../asserts/Group 2034@2x.png"
import { menuTitle, profileItem } from "../../constants/Header"
import clsx from "clsx"
import "../../styles/Header.scss"
import { Link } from "react-router-dom"
import { useHistory } from "react-router-dom"
// import s from '../../asserts/Group 2034@2x.png'
const useStyles = makeStyles((theme) => ({
	title: {
		width: "60%",
		display: "flex",
		justifyContent: "flex-start",
		alignItems: "center",
	},
	toolbar: {
		backgroundColor: "#2A2C5A",
		height: 64,
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",
	},
	toolbarGutters: {
		paddingLeft: 0,
		paddingRight: 10,
	},
	profileContainer: {
		width: "11.05%",
		display: "flex",
		justifyContent: "space-between",
		alignItems: "center",
	},
	profileText: {
		fontSize: 14,
		color: "#FFFFFF",
		fontFamily: "Rubik",
	},
	profileMenu: {
		marginTop: 10,
		fontFamily: "Dosis",
		fontSize: 14,
		textAlign: "left",
	},
	menuItemText: {
		fontSize: 14,
		fontFamily: "Dosis",
		color: "#0000008A",
	},
}))

export default function MenuAppBar(props) {
	const classes = useStyles()
	const history = useHistory()
	const [anchorEl, setAnchorEl] = React.useState(null)
	const [itemVisssible, setItemVissible] = useState({
		vissibleItem: [],
		status: null,
		key: null,
	})
	const [active, setActive] = useState(0)
	const open = Boolean(anchorEl)
	console.log(history.location.pathname.includes(itemVisssible.key))
	const handleMenu = (event, status, ind) => {
		console.log(ind)
		if (status === "menuItemVissible") {
			setItemVissible({
				...itemVisssible,
				vissibleItem: menuTitle[ind].menuItem,
				status: status,
				key: menuTitle[ind].key,
			})
		} else if (status === "profileItemVissible") {
			setItemVissible({
				...itemVisssible,
				vissibleItem: profileItem,
				status: status,
			})
		}
		setAnchorEl(event.currentTarget)
	}

	const handleClose = () => {
		setAnchorEl(null)
	}
	const changeActive = () => {
		setActive(itemVisssible.key)
		setAnchorEl(null)
	}
	console.log(active)
	return (
		<div className={classes.root}>
			<Toolbar
				className={classes.toolbar}
				classes={{ gutters: classes.toolbarGutters }}
			>
				<Box className={classes.title}>
					<img src={HeasderImg} style={{ height: 64, width: 100 }} />

					{menuTitle.map((title, index) => (
						<Button
							variant='text'
							className={clsx({
								navlink: true,
								navlinkActive: history.location.pathname.includes(title.key),
							})}
							key={index}
							onClick={(e) => handleMenu(e, "menuItemVissible", index)}
						>
							{title.titleName}
						</Button>
					))}
				</Box>

				<div
					className={classes.profileContainer}
					onClick={(e) => handleMenu(e, "profileItemVissible", null)}
				>
					<div>
						<Typography variant='subtitle1' className={classes.profileText}>
							Name
						</Typography>
						<Typography
							variant='inherit'
							className={classes.profileText}
							style={{ color: "#FFFFFF8A", fontSize: 12 }}
						>
							Role
						</Typography>
					</div>
					<IconButton
						aria-label='account of current user'
						aria-controls='menu-appbar'
						aria-haspopup='true'
						color='inherit'
					>
						<Avatar alt='hjh' src={vaduvelu}></Avatar>
					</IconButton>
				</div>
				<Menu
					id='menu-appbar'
					anchorEl={anchorEl}
					getContentAnchorEl={null}
					keepMounted
					elevation={1}
					anchorOrigin={{
						vertical: "bottom",
						horizontal: "center",
					}}
					transformOrigin={{
						vertical: "top",
						horizontal: "center",
					}}
					open={open}
					onClose={handleClose}
					className={classes.profileMenu}
				>
					{itemVisssible.status === "profileItemVissible"
						? itemVisssible.vissibleItem.map((item, index) => (
								<MenuItem
									onClick={handleClose}
									key={index}
									className={classes.menuItemText}
								>
									{item.profiletext}
								</MenuItem>
						  ))
						: itemVisssible.vissibleItem.map((item, index) => (
								<Link
									to={item.subMenuItemPath}
									style={{ textDecoration: "none" }}
								>
									<MenuItem
										onClick={() => changeActive()}
										key={index}
										className={classes.menuItemText}
									>
										{item.subMenuItemName}
									</MenuItem>
								</Link>
						  ))}
				</Menu>
			</Toolbar>
		</div>
	)
}
