import React from "react"
import TextField from "@material-ui/core/TextField"

function TextFields(props) {
	console.log(props.onClick)
	return (
		<div style={props.style} onClick={() => props.onClick()}>
			<TextField
				type={props.type}
				id='outlined-basic'
				label={props.label}
				variant={props.variant}
				error
				className={props.className}
				placeholder={props.placeholder}
				onChange={props.onChange}
				name={props.name}
				value={props.value}
				disabled={props.disabled}
				helperText={props.helperText}
				FormHelperTextProps={props.FormHelperTextProps}
				fullWidth
			/>
		</div>
	)
}

export { TextFields }
