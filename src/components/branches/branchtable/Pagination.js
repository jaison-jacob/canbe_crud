import React from "react"
import TablePagination from "@material-ui/core/TablePagination"

export default function Pagination({
	pageIndex,
	nextPage,
	previousPage,
	setPageSize,
	pageSize,
	pageOptions,
}) {
	const handleChangePage = (event, newPage) => {
		if (pageIndex > newPage) {
			previousPage()
		} else {
			nextPage()
		}
	}

	const handleChangeRowsPerPage = async (event) => {
		setPageSize(parseInt(event.target.value, 10))
	}

	return (
		<TablePagination
			component='div'
			count={pageOptions.length * 10}
			page={pageIndex}
			onChangePage={handleChangePage}
			rowsPerPage={pageSize}
			onChangeRowsPerPage={handleChangeRowsPerPage}
			rowsPerPageOptions={[5, 10, 25]}
		/>
	)
}
