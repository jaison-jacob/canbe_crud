import React from "react"
import IconButton from "@material-ui/core/IconButton"
import { makeStyles } from "@material-ui/core/styles"
import Menu from "@material-ui/core/Menu"
import MenuItem from "@material-ui/core/MenuItem"
import MoreHorizIcon from "@material-ui/icons/MoreHoriz"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import * as Actions from "../../../actions/branches/ListData"
import { useHistory } from "react-router-dom"

const useStyles = makeStyles({
	root: {
		width: 230,
	},
})

const options = ["View Detail", "Edit", "Inactive"]

const ITEM_HEIGHT = 48

function ProfilePopUp(props) {
	console.log(props)
	let history = useHistory()
	const classes = useStyles()
	const [anchorEl, setAnchorEl] = React.useState(null)
	const open = Boolean(anchorEl)

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget)
	}

	const handleClose = () => {
		setAnchorEl(null)
	}

	const l = async (option, index) => {
		console.log(option)
		if (option === "Inactive") {
			props.InActivefun(index)
		} else {
			props.Editfun(index, option)
			history.push("/branchNewForm")
		}
	}

	return (
		<div>
			<IconButton
				aria-label='more'
				aria-controls='long-menu'
				aria-haspopup='true'
				onClick={handleClick}
			>
				<MoreHorizIcon />
			</IconButton>
			<Menu
				id='long-menu'
				anchorEl={anchorEl}
				keepMounted
				open={open}
				onClose={handleClose}
				PaperProps={{
					style: {
						maxHeight: ITEM_HEIGHT * 4.5,
						width: "180px",
						fontSize: 2,
					},
				}}
			>
				{options.map((option) => (
					<MenuItem
						key={props.index}
						selected={option === "Pyxis"}
						onClick={handleClose}
						className='profilePopupIteme'
						onClick={() => l(option, props.index)}
					>
						{option}
					</MenuItem>
				))}
			</Menu>
		</div>
	)
}
const mapDispatchToprops = (dispatch) => {
	console.log(dispatch)
	return bindActionCreators(
		{
			Editfun: Actions.Edit,
			InActivefun: Actions.inActiveProfile,
		},
		dispatch
	)
}
const mapStateToProps = (state) => {
	console.log(state)
	return {
		value: { ...state },
	}
}

export default connect(mapStateToProps, mapDispatchToprops)(ProfilePopUp)
