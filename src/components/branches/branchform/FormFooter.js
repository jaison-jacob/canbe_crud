import React from "react"
import { Button } from "@material-ui/core"

function FormFooter(props) {
	return (
		<div>
			<div className='Buttun_style'>
				<Button
					variant='outlined'
					style={{ color: "#707070", fontFamily: "Lato" }}
					onClick={() => props.create_new_data()}
				>
					Submit
				</Button>
				<Button
					variant='outlined'
					style={{
						marginLeft: "3%",
						color: "#707070",
						fontFamily: "Lato",
					}}
				>
					Cancel
				</Button>
			</div>
		</div>
	)
}

export default FormFooter
