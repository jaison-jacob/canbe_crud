import React from "react"
import InputLabel from "@material-ui/core/InputLabel"
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import TextField from "@material-ui/core/TextField"
import { makeStyles } from "@material-ui/core"
import MenuItem from "@material-ui/core/MenuItem"
import FormGroup from "@material-ui/core/FormGroup"
import Radio from "@material-ui/core/Radio"
import RadioGroup from "@material-ui/core/RadioGroup"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import FormLabel from "@material-ui/core/FormLabel"
import Checkbox from "@material-ui/core/Checkbox"
import { withStyles } from "@material-ui/core/styles"
import clsx from "clsx"
import "../../../styles/Header.scss"
import {
	SelectField,
	FormWrapper,
	TextFields,
	RadioField,
} from "../../reusable"

export const useStyles = makeStyles((theme) => ({
	select_Style: {
		minWidth: 120,
		"& .MuiOutlinedInput-input": {
			color: "#707070",
			fontSize: 16,
			fontFamily: "Lato",
		},
		"& .MuiInputLabel-root": {
			color: "#707070",
		},
		"& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
			borderColor: "#707070",
		},
		"&:hover .MuiOutlinedInput-input": {
			color: "#707070",
		},
		"&:hover .MuiInputLabel-root": {
			color: "#707070",
		},
		"&:hover .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
			borderColor: "#707070",
		},
		"& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-input": {
			color: "#000000DE",
		},
		"& .MuiInputLabel-root.Mui-focused": {
			color: "#707070",
		},
		"& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
			borderColor: "#707070",
		},
	},
	textinputStyle: {
		"& .MuiOutlinedInput-input": {
			color: "#0000008A",
			fontSize: 16,
			fontFamily: "Lato",
		},
		"& .MuiInputLabel-root": {
			color: "#707070",
		},
		"& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
			borderColor: "#0000008A",
		},
		"&:hover .MuiOutlinedInput-input": {
			color: "#707070",
		},
		"&:hover .MuiInputLabel-root": {
			color: "#707070",
		},
		"&:hover .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
			borderColor: "#707070",
		},
		"& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-input": {
			color: "#000000DE",
		},
		"& .MuiInputLabel-root.Mui-focused": {
			color: "#707070",
		},
		"& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
			borderColor: "#707070",
		},
	},
	textstyle: {
		width: "100%",
	},
	formControl: {
		display: "flex",
		flexWrap: "wrap",
		justifyContent: "space-between",
	},
	radio_style: {
		marginLeft: 20,
	},
	radio_Text_Style: {
		fontFamily: "Lato",
		fontSize: 16,
		color: "#0000008A",
	},
	radio_Label_Style: {
		fontFamily: "Lato",
		color: "#707070",
		fontSize: 16,
	},
	chekbox_style: {
		marginBottom: 20,
	},
	formControl1: {
		minWidth: 120,
	},
	helperText: {
		fontSize: 16,
		fontFamily: "Lato",
	},
}))

const BlackCheckBox = withStyles({
	root: {
		color: "black",
		"&$checked": {
			color: "black",
		},
	},
	checked: {},
})((props) => <Checkbox color='default' {...props} />)

function FormCreation(props) {
	const classes = useStyles()
	return (
		<FormWrapper>
			<SelectField
				value={props.newBranchData.arganizationName.value || ""}
				onChange={props.onchange_Form_Data}
				label='Arganization Name'
				style={{ width: "47%" }}
				name='arganizationName'
				disabled={props.newBranchData.arganizationName.disable}
				options={["None", "loyola", "Dmi", "Virthitech"]}
				className={clsx(classes.select_Style, "form_Text_Style")}
			/>
			<TextFields
				style={{ width: "47%", marginBottom: 15 }}
				onClick={() => props.setTouched("branchName")}
				type='text'
				label='Branch Name'
				variant='outlined'
				className={classes.textinputStyle}
				onChange={props.onchange_Form_Data}
				name='branchName'
				value={props.newBranchData.branchName.value}
				disabled={props.newBranchData.branchName.disable}
				helperText={props.newBranchData.branchName.errorMsg}
				FormHelperTextProps={{ classes: { root: classes.helperText } }}
				fullWidth
			/>
			<div className='lineStyle' />

			<FormControl component='fieldset' className={classes.chekbox_style}>
				<FormGroup>
					{["Mark this as a Corporate/Main Address"].map((e) => (
						<FormControlLabel
							control={
								<BlackCheckBox
									checked={e}
									onChange={props.onchange_Form_Data}
									name={e}
								/>
							}
							label={
								<span
									style={{
										fontSize: 16,
										color: "#000000DE",
										fontFamily: "Lato",
									}}
								>
									{e || ""}
								</span>
							}
						/>
					))}
				</FormGroup>
			</FormControl>

			<TextFields
				style={{ width: "100%", marginBottom: 25 }}
				onClick={() => props.setTouched("branchAddress")}
				type='text'
				variant='outlined'
				placeholder='Branch Address'
				className={classes.textinputStyle}
				onChange={props.onchange_Form_Data}
				name='branchAddress'
				value={props.newBranchData.branchAddress.value}
				disabled={props.newBranchData.branchAddress.disable}
				helperText={props.newBranchData.branchAddress.errorMsg}
				FormHelperTextProps={{ classes: { root: classes.helperText } }}
			/>

			<SelectField
				value={props.newBranchData.Country.value || ""}
				onChange={props.onchange_Form_Data}
				label='Country'
				name='Country'
				disabled={props.newBranchData.Country.disable}
				options={["India", "Oversease"]}
				className={clsx(classes.select_Style, "form_Text_Style")}
				style={{ width: "31.64%" }}
			/>

			{props.newBranchData.Country.value !== "India" && (
				<TextFields
					style={{ width: "58.22%", marginBottom: 25 }}
					onClick={() => props.setTouched("City")}
					type='text'
					placeholder='City'
					variant='outlined'
					className={classes.textinputStyle}
					onChange={props.onchange_Form_Data}
					name='City'
					value={props.newBranchData.City.value}
					disabled={props.newBranchData.City.disable}
					helperText={props.newBranchData.City.errorMsg}
					FormHelperTextProps={{ classes: { root: classes.helperText } }}
				/>
			)}

			{props.newBranchData.Country.value === "India" && (
				<>
					<SelectField
						value={props.newBranchData.Country.value || ""}
						onChange={props.onchange_Form_Data}
						value={props.newBranchData.State.value || ""}
						onChange={props.onchange_Form_Data}
						label='State'
						name='State'
						disabled={props.newBranchData.State.disable}
						options={["Kannyakumari", "Kerala", "Chennai", "Madurai", "Trichy"]}
						className={clsx(classes.select_Style, "form_Text_Style")}
						style={{ width: "31.64%" }}
					/>

					<TextFields
						style={{ width: "26.58%", marginBottom: 25 }}
						onClick={() => props.setTouched("Pincode")}
						type='Number'
						placeholder='Pincode'
						variant='outlined'
						className={classes.textinputStyle}
						onChange={props.onchange_Form_Data}
						name='Pincode'
						value={props.newBranchData.Pincode.value}
						disabled={props.newBranchData.Pincode.disable}
						helperText={props.newBranchData.Pincode.errorMsg}
						FormHelperTextProps={{ classes: { root: classes.helperText } }}
					/>
				</>
			)}

			<TextFields
				style={{ width: "47%", marginBottom: 25 }}
				onClick={() => props.setTouched("Longitude")}
				type='text'
				placeholder='Longitude'
				variant='outlined'
				className={classes.textinputStyle}
				onChange={props.onchange_Form_Data}
				name='Longitude'
				value={props.newBranchData.Longitude.value}
				disabled={props.newBranchData.Longitude.disable}
				helperText={props.newBranchData.Longitude.errorMsg}
				FormHelperTextProps={{ classes: { root: classes.helperText } }}
			/>
			<TextFields
				style={{ width: "47%", marginBottom: 25 }}
				onClick={() => props.setTouched("LandMark")}
				type='text'
				id='outlined-basic'
				placeholder='LandMark'
				variant='outlined'
				className={classes.textinputStyle}
				onChange={props.onchange_Form_Data}
				name='LandMark'
				value={props.newBranchData.LandMark.value}
				disabled={props.newBranchData.LandMark.disable}
				helperText={props.newBranchData.LandMark.errorMsg}
				FormHelperTextProps={{ classes: { root: classes.helperText } }}
			/>

			<TextFields
				style={{ width: "47%", marginBottom: 25 }}
				onClick={() => props.setTouched("distanceFromLandMark")}
				type='text'
				placeholder='Distance From LandMark'
				variant='outlined'
				className={classes.textinputStyle}
				onChange={props.onchange_Form_Data}
				name='distanceFromLandMark'
				value={props.newBranchData.distanceFromLandMark.value}
				disabled={props.newBranchData.distanceFromLandMark.disable}
				helperText={props.newBranchData.distanceFromLandMark.errorMsg}
				FormHelperTextProps={{ classes: { root: classes.helperText } }}
			/>
			<TextFields
				style={{ width: "47%", marginBottom: 25 }}
				onClick={() => props.setTouched("Nearest")}
				type='text'
				placeholder='Nearest Bus Stop/RaiWay Station'
				variant='outlined'
				className={classes.textinputStyle}
				onChange={props.onchange_Form_Data}
				name='Nearest'
				value={props.newBranchData.Nearest.value}
				disabled={props.newBranchData.Nearest.disable}
				helperText={props.newBranchData.Nearest.errorMsg}
				FormHelperTextProps={{ classes: { root: classes.helperText } }}
			/>
			<div className='lineStyle' />

			<RadioField
				label={"Status"}
				classes={{ root: classes.radio_style }}
				className={classes.radio_Label_Style}
				name='Status'
				value={props.newBranchData.Status.value || ""}
				onChange={props.onchange_Form_Data}
				options={["Active", "Deactive"]}
				labelClassname={classes.radio_Text_Style}
				disabled={props.newBranchData.Status.disable}
			/>
			<TextFields
				style={{ width: "47%", marginBottom: 25 }}
				onClick={() => props.setTouched("effectiveDate")}
				type='date'
				placeholder='Effective Date'
				variant='outlined'
				className={classes.textinputStyle}
				onChange={props.onchange_Form_Data}
				name='effectiveDate'
				value={props.newBranchData.effectiveDate.value}
				disabled={props.newBranchData.effectiveDate.disable}
				helperText={props.newBranchData.effectiveDate.errorMsg}
				FormHelperTextProps={{ classes: { root: classes.helperText } }}
			/>
			{props.newBranchData.Status.value === "Deactive" && (
				<TextFields
					style={{ width: "100%", marginBottom: 25 }}
					onClick={() => props.setTouched("reasonForDeactive")}
					type='text'
					placeholder='Reason For Deactive'
					variant='outlined'
					className={classes.textinputStyle}
					onChange={props.onchange_Form_Data}
					name='reasonForDeactive'
					value={props.newBranchData.reasonForDeactive.value}
					disabled={props.newBranchData.reasonForDeactive.disable}
					helperText={props.newBranchData.reasonForDeactive.errorMsg}
					FormHelperTextProps={{ classes: { root: classes.helperText } }}
				/>
			)}
		</FormWrapper>
	)
}

export default FormCreation
