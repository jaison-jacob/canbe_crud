import { Button, IconButton, makeStyles } from "@material-ui/core"
import React, { useEffect, useState } from "react"
import ArrowBackIcon from "@material-ui/icons/ArrowBack"
import FormCreation from "./FormCreation"
import { FormData } from "../../../utills/FormData"
import { branchformValidation } from "../../../validation/branch/Form"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import * as Actions from "../../../actions/branches/ListData"
import { useHistory } from "react-router-dom"
import FormFooter from "./FormFooter"
import "../../../styles/BranchFormComp.scss"

const useStyles = makeStyles({
	arrowIcon: {
		color: "#333333",
	},
})

function BranchFormComponent(props) {
	console.log(props)

	const classes = useStyles()
	let history = useHistory()
	const [newBranchData, setNewBranchData] = useState({
		arganizationName: {
			value: "loyola",
			touched: false,
			errorMsg: "",
		},
		branchName: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		branchAddress: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		Country: {
			value: "India",
			touched: false,
			errorMsg: "",
		},
		City: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		State: {
			value: "Chennai",
			touched: false,
			errorMsg: "",
		},
		Pincode: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		Latitude: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		Longitude: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		LandMark: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		distanceFromLandMark: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		Nearest: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		Status: {
			value: "Active",
		},
		effectiveDate: {
			value: "",
			touched: false,
			errorMsg: "",
		},
		reasonForDeactive: {
			value: "",
			touched: false,
			errorMsg: "",
		},
	})
	const onchange_Form_Data = async (event) => {
		console.log(event.target.value)
		let copyNewBranchData = { ...newBranchData }
		copyNewBranchData[event.target.name].value = event.target.value.trim()
		await setNewBranchData({
			...copyNewBranchData,
		})
		let message = await branchformValidation(
			newBranchData[event.target.name],
			event.target.name
		)
		console.log(message)
		copyNewBranchData[event.target.name].errorMsg = message
		await setNewBranchData({
			...copyNewBranchData,
		})
	}

	const setTouched = (name) => {
		console.log(name)
		let copyNewBranchData = { ...newBranchData }
		copyNewBranchData[name].touched = true
		if (!copyNewBranchData[name].value && !props.value.profileView) {
			copyNewBranchData[name].errorMsg = "please Enter the data"
		}

		setNewBranchData({
			...copyNewBranchData,
		})
	}

	const create_new_data = async () => {
		if (!!props.value.edit) {
			props.createListData(newBranchData, "edit")
		} else {
			for (const item in newBranchData) {
				if (newBranchData[item].errorMsg) {
					return
				}
			}
			console.log("submit")
			props.createListData(newBranchData, "add")
		}

		await history.push("/")
	}

	useEffect(() => {
		if (!!props.value.edit) {
			console.log("hello")
			let copyNewBranchData = { ...newBranchData }
			for (const [key, value] of Object.entries(props.value.edit)) {
				copyNewBranchData[key].value = value
				copyNewBranchData[key].disable = props.value.profileView
			}

			setNewBranchData(copyNewBranchData)
		}
	}, [])
	const gotoBranchList = () => {
		let copyNewBranchData = { ...newBranchData }
		for (const item in newBranchData) {
			copyNewBranchData[item].disable = false
		}

		setNewBranchData(copyNewBranchData)
		history.push("/")
	}

	return (
		<div>
			<div className='formNav'>
				<IconButton
					classes={{ root: classes.arrowIcon }}
					onClick={() => gotoBranchList()}
				>
					<ArrowBackIcon />
				</IconButton>
				<p>NEW BRANCH</p>
			</div>
			<div className='form'>
				<div className='formContainer'>
					<div className='formInsideContainer'>
						<FormCreation
							FormData={FormData}
							onchange_Form_Data={onchange_Form_Data}
							newBranchData={{ ...newBranchData }}
							setTouched={setTouched}
						/>
						{!props.value.profileView && (
							<FormFooter create_new_data={create_new_data} />
						)}
					</div>
				</div>
			</div>
		</div>
	)
}

const mapDispatchToprops = (dispatch) => {
	return bindActionCreators(
		{
			createListData: Actions.createListData,
		},
		dispatch
	)
}
const mapStateToProps = (state) => {
	console.log(state)
	return {
		value: { ...state.branchListReducer },
	}
}

export default connect(mapStateToProps, mapDispatchToprops)(BranchFormComponent)
