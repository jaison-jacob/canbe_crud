import { combineReducers } from "redux"
import { branchListReducer } from "./actions/branches/ListData"

export const reducers = combineReducers({
	branchListReducer,
})
